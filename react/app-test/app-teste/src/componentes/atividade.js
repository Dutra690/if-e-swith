import React from "react"


import Tabela01 from './tabela01'
import Tabela02 from './tabela02'
import Tabela03 from './tabela03'



const Tabela = () => {

    return(

        <table border = "1" cellpadding = "10">

            <Tabela01 />
            <Tabela02 />
            <Tabela03 />
            
        </table>

    )
        

}

export default Tabela