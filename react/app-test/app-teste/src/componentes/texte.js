import React from 'react';

const Input = props => {
    return(
        <p>
            <label>{props.label}</label>
            <input type={props.type} />
        </p>
    )
}

export default Input;



--------------------------------------------------------------------


import React from 'react';

import Input from './Input';

const Formulario = () => {
    return(
        <form>
            <Input label="Nomeeeee" type="text" />
            <Input label="Email" type="email" />
            <Input label="Telefone" />
            <Input label="RG" />
        </form>
    )
}

export default Formulario;


