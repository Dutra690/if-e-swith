import React from "react"
import Lista01 from "./lista01"
import Lista02 from "./lista02"
import Lista03 from "./lista03"



const Usuario = () => {

    return(
        <ul>
            <Lista01 />
            <Lista02 />
            <Lista03 />
        </ul>
    )
}

export default Usuario