import React from "react"


const Input = props => {

    return(
        <p>
            <label>{props.label}</label>
            <input type={props.type}/>
        </p>
        
    )
}

export default Input