import React from 'react'
import './App.css'

import './componentes/atividade'
import Tabela from './componentes/atividade'
import Imagen from './componentes/img'
import Form  from './componentes/formulario'
import Usuario from './componentes/usuario'




const App = () => {
  return (
    <div className="App">
      <h1>Hello World!!</h1>
      <Tabela />
      <Usuario />
      <Form />
      <Imagen />
    </div>
 
    
  )
}

export default App;
